package com.parliament.vote.domain;

import com.parliament.vote.enums.SzavazasiTipus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Entity
@Getter
@Setter
public class Szavazas {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String szavazasId;
    private LocalDateTime idopont;
    private String targy;
    private SzavazasiTipus tipus;
    private String elnok;

    @OneToMany(mappedBy = "szavazas", cascade = CascadeType.ALL)
    private List<Szavazat> szavazatok;

    public Szavazas(){}

    public Szavazas(String idopont, String targy, SzavazasiTipus tipus, String elnok){
        this.idopont = convertStringToLocalDateTime(idopont);
        this.targy = targy;
        this.tipus = tipus;
        this.elnok = elnok;
    }

    public static LocalDateTime convertStringToLocalDateTime(String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return LocalDateTime.parse(dateString, formatter);
    }


}
