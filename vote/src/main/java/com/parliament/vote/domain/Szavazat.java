package com.parliament.vote.domain;

import com.parliament.vote.enums.SzavazatTipus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Szavazat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String kepviselo;
    private SzavazatTipus szavazat;

    @ManyToOne
    @JoinColumn(name="szavazas_id")
    private Szavazas szavazas;

    public Szavazat(){}

    public Szavazat(String kepviselo, SzavazatTipus szavazat){
        this.kepviselo = kepviselo;
        this.szavazat = szavazat;
    }
}
