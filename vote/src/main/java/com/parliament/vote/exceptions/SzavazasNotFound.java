package com.parliament.vote.exceptions;

public class SzavazasNotFound extends RuntimeException{
    public SzavazasNotFound(String message){
        super(message);
    }
}
