package com.parliament.vote.exceptions;

public class KepviseloDidNotVoteInSzavazas extends RuntimeException{
    public KepviseloDidNotVoteInSzavazas(String message){
        super(message);
    }
}
