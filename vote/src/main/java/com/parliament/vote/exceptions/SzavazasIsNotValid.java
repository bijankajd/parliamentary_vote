package com.parliament.vote.exceptions;

public class SzavazasIsNotValid extends RuntimeException{
    public SzavazasIsNotValid(String message){
        super(message);
    }
}

