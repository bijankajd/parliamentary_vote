package com.parliament.vote.exceptionHandler;

import com.parliament.vote.exceptions.KepviseloDidNotVoteInSzavazas;
import com.parliament.vote.exceptions.SzavazasIsNotValid;
import com.parliament.vote.exceptions.SzavazasNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(KepviseloDidNotVoteInSzavazas.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleKepviseloDidNotVoteInSzavazasException(KepviseloDidNotVoteInSzavazas ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(SzavazasIsNotValid.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public String handleSzavazasIsNotValidException(SzavazasIsNotValid ex) {
        return ex.getMessage();
    }

    @ExceptionHandler(SzavazasNotFound.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String handleSzavazasNotFoundException(SzavazasNotFound ex) {
        return ex.getMessage();
    }
}
