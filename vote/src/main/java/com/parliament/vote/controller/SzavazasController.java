package com.parliament.vote.controller;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.dto.SzavazasDto;
import com.parliament.vote.response.KepviseloSzavazatResponse;
import com.parliament.vote.response.NewSzavazasResponse;
import com.parliament.vote.response.SzavazasResultResponse;
import com.parliament.vote.service.SzavazasResultService;
import com.parliament.vote.service.SzavazasService;
import com.parliament.vote.service.SzavazatOfKepviseloService;
import com.parliament.vote.service.SzavazatService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/szavazasok")
public class SzavazasController {

    @Autowired
    private SzavazasService szavazasService;
    @Autowired
    private SzavazatOfKepviseloService szavazatOfKepviseloService;
    @Autowired
    private SzavazasResultService szavazasResultService;
    @Autowired
    private SzavazatService szavazatService;


    // 1. feladatresz
    @PostMapping(value = "/szavazas", produces = "application/json")
    public ResponseEntity<NewSzavazasResponse> addSzavazas(@Valid @RequestBody SzavazasDto szavazasDto){
        NewSzavazasResponse szavazasId = szavazasService.addSzavazas(szavazasDto);
        return new ResponseEntity<>(szavazasId, HttpStatus.CREATED);
    }

    // 2.feladatrész
    @GetMapping("/szavazat")
    public ResponseEntity<KepviseloSzavazatResponse> getSzavazatOfKepviselo(@RequestParam("szavazas") String szavazas, @RequestParam("kepviselo") String kepviselo){
        KepviseloSzavazatResponse kepviseloSzavazat = szavazatOfKepviseloService.getSzavzatOfKepviselo(szavazas, kepviselo);
        return new ResponseEntity<>(kepviseloSzavazat, HttpStatus.OK);
    }

    // 3. feladatresz
    @GetMapping("/eredmeny")
    public ResponseEntity<SzavazasResultResponse> getResult(@RequestParam("szavazas") String szavazas){
        SzavazasResultResponse result =  szavazasResultService.getResultOfSzavazas(szavazas);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

}
