package com.parliament.vote.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class NewSzavazasResponse {
    String szavazasId;
}
