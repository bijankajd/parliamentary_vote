package com.parliament.vote.dto;

import com.parliament.vote.domain.Szavazat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Data
@AllArgsConstructor
public class SzavazasDto {
    @NotBlank(message = "idopont is required")
    private String idopont;
    @NotBlank(message = "targy is required")
    private String targy;
    @NotBlank(message = "tipus is required")
    @Pattern(regexp = "^[emj]$", message = "tipus is not valid")
    private String tipus;
    @NotBlank(message = "elnok is required")
    private String elnok;
    @NotNull(message = "szavazatok is required")
    private List<Szavazat> szavazatok;
}
