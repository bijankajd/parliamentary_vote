package com.parliament.vote.databaseMigration;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.enums.SzavazasiTipus;
import com.parliament.vote.enums.SzavazatTipus;
import com.parliament.vote.repo.SzavazasRepo;
import com.parliament.vote.repo.SzavazatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DbInit implements CommandLineRunner {
    @Autowired
    private SzavazatRepo szavazatRepo;
    @Autowired
    private SzavazasRepo szavazasRepo;

    @Override
    public void run(String... args) throws Exception {

        Szavazat szavazat1 = new Szavazat("kepviselo1", SzavazatTipus.t);
        Szavazat szavazat2 = new Szavazat("kepviselo2", SzavazatTipus.i);
        Szavazat szavazat3 = new Szavazat("kepviselo3", SzavazatTipus.i);
        Szavazat szavazat4 = new Szavazat("kepviselo4", SzavazatTipus.n);
        Szavazat szavazat5 = new Szavazat("kepviselo5", SzavazatTipus.n);
        Szavazat szavazat6 = new Szavazat("kepviselo6", SzavazatTipus.n);
        Szavazat szavazat7 = new Szavazat("kepviselo7", SzavazatTipus.n);

        List<Szavazat> szavazatok = new ArrayList<>();
        szavazatok.add(szavazat1);

        for(Szavazat szavazat : szavazatok){
            szavazatRepo.save(szavazat);
        }


        Szavazas szavazas1 = new Szavazas("2023-12-13T14:30:00Z", "targy1", SzavazasiTipus.j, "kepviselo1");
        szavazasRepo.save(szavazas1);


        szavazas1.setTargy("targy2");
        szavazas1.setSzavazatok(szavazatok);
        szavazasRepo.save(szavazas1);

    }
}
