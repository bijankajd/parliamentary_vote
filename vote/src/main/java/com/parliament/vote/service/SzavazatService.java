package com.parliament.vote.service;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.enums.SzavazatTipus;
import com.parliament.vote.repo.SzavazatRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SzavazatService {
    @Autowired
    private SzavazatRepo szavazatRepo;

    public List<Szavazat> findBySzavazasAndSzavazatTipus(Szavazas szavazas, SzavazatTipus szavazatTipus){
        return szavazatRepo.findBySzavazasAndSzavazat(szavazas, szavazatTipus);
    }

    public Szavazat addSzavazat(Szavazat szavazat){
        return szavazatRepo.save(szavazat);
    }

    public List<Szavazat> getAll(){
        return szavazatRepo.findAll();
    }
}
