package com.parliament.vote.service;

import com.parliament.vote.domain.Constants;
import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.enums.SzavazatTipus;
import com.parliament.vote.response.SzavazasResultResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SzavazasResultService {
    @Autowired
    private SzavazasService szavazasService;
    @Autowired
    private SzavazatService szavazatService;

    public SzavazasResultResponse getResultOfSzavazas(String szavazasId){
        Szavazas szavazas = szavazasService.findBySzavazasId(szavazasId);
        return new SzavazasResultResponse(
            getEredmeny(szavazas.getTipus().name(), szavazas),
                getVotedKepviselokSzama(szavazas),
                getIgenekSzama(szavazas),
                getNemekSzama(szavazas),
                getTartozkodasokSzama(szavazas)
        );
    }


    private String getEredmeny(String szavazatiTipus, Szavazas szavazas){
        switch (szavazatiTipus) {
            case "j" -> {
                return Constants.ACCEPTED;
            }
            case "e" -> {
                if (isHalfOfThosePresentVotedYes(szavazas)) {
                    return Constants.ACCEPTED;
                }
                return Constants.REJECTED;
            }
            case "m" -> {
                if (isHalfOfAllKepviseloVotedYes(szavazas)) {
                    return Constants.ACCEPTED;
                }
                return Constants.REJECTED;
            }
            default -> {
                return Constants.REJECTED;
            }
        }
    }

    private int getVotedKepviselokSzama(Szavazas szavazas){
       return szavazas.getSzavazatok().size();
    }

    private int getIgenekSzama(Szavazas szavazas){
        SzavazatTipus igen = SzavazatTipus.i;
        return szavazatService.findBySzavazasAndSzavazatTipus(szavazas, igen).size();
    }

    private int getNemekSzama(Szavazas szavazas){
        SzavazatTipus nem = SzavazatTipus.n;
        return szavazatService.findBySzavazasAndSzavazatTipus(szavazas, nem).size();
    }

    private int getTartozkodasokSzama(Szavazas szavazas){
        SzavazatTipus tartozkodas = SzavazatTipus.t;
        return szavazatService.findBySzavazasAndSzavazatTipus(szavazas, tartozkodas).size();
    }

    private boolean isHalfOfThosePresentVotedYes(Szavazas szavazas){
        SzavazatTipus igen = SzavazatTipus.i;
        return (szavazas.getSzavazatok().size() / 2) <= szavazatService.findBySzavazasAndSzavazatTipus(szavazas, igen).size();
    }

    private boolean isHalfOfAllKepviseloVotedYes(Szavazas szavazas){
        SzavazatTipus igen = SzavazatTipus.i;
        return (Constants.NUMBER_OF_KEPVISELOK / 2) <=  szavazatService.findBySzavazasAndSzavazatTipus(szavazas, igen).size();
    }

}
