package com.parliament.vote.service;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.dto.SzavazasDto;
import com.parliament.vote.enums.SzavazasiTipus;
import com.parliament.vote.exceptions.SzavazasIsNotValid;
import com.parliament.vote.exceptions.SzavazasNotFound;
import com.parliament.vote.repo.SzavazasRepo;
import com.parliament.vote.response.NewSzavazasResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.parliament.vote.domain.Szavazas.convertStringToLocalDateTime;

@Service
public class SzavazasService {
    @Autowired
    private SzavazasRepo szavazasRepo;

    @Autowired
    private SzavazatService szavazatService;

    public NewSzavazasResponse addSzavazas(SzavazasDto szavazasDto){
        LocalDateTime idopont = convertStringToLocalDateTime(szavazasDto.getIdopont());

        if(isSzavazasRegistratedOnSameDate(idopont)){
            throw new SzavazasIsNotValid("Szavazas is already registered on this date");
        }
        else if(elnokHasNotGotSzavazat(szavazasDto.getSzavazatok(), szavazasDto.getElnok())){
            throw new SzavazasIsNotValid("Elnok has not got a szavazat");
        }
        else if(hasMultipleKepviselo(szavazasDto.getSzavazatok())){
            throw new SzavazasIsNotValid("There is multiple szavazat for a kepviselo");
        }
        Szavazas szavazas = new Szavazas(
                szavazasDto.getIdopont(),
                szavazasDto.getTargy(),
                getSzavazasiTipus(szavazasDto.getTipus()),
                szavazasDto.getElnok()
        );


        for(Szavazat szavazat : szavazasDto.getSzavazatok()){
            szavazatService.addSzavazat(szavazat);
        }

        szavazas.setSzavazatok(szavazasDto.getSzavazatok());
        return new NewSzavazasResponse(szavazasRepo.save(szavazas).getSzavazasId());
    }

    public List<Szavazas> getAll(){
        return szavazasRepo.findAll();
    }

    public Szavazas findBySzavazasId(String szavazasId){
        if(szavazasRepo.findBySzavazasId(szavazasId).isEmpty()){
            throw new SzavazasNotFound("Szavazas by id " + szavazasId + " not found");
        }
        return szavazasRepo.findBySzavazasId(szavazasId).get();
    }

    private boolean isSzavazasRegistratedOnSameDate(LocalDateTime idopont){
        return szavazasRepo.findByIdopont(idopont).isPresent();
    }

    private boolean elnokHasNotGotSzavazat(List<Szavazat> szavazatok, String elnok){
        for(Szavazat szavazat : szavazatok){
            if(szavazat.getKepviselo().equals(elnok)){
                return false;
            }
        }
        return true;
    }

    private boolean hasMultipleKepviselo(List<Szavazat> szavazatok){
        Set<String> uniqueKepviselos = new HashSet<>();

        for (Szavazat szavazat : szavazatok) {
            uniqueKepviselos.add(szavazat.getKepviselo());
        }

        return uniqueKepviselos.size() < szavazatok.size();
    }

    private SzavazasiTipus getSzavazasiTipus(String tipus){
        switch (tipus) {
            case "j" -> {
                return SzavazasiTipus.j;
            }
            case "e" -> {
                return SzavazasiTipus.e;
            }
            default -> {
                return SzavazasiTipus.m;
            }
        }
    }

}
