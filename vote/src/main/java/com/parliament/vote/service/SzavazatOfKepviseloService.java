package com.parliament.vote.service;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.exceptions.KepviseloDidNotVoteInSzavazas;
import com.parliament.vote.response.KepviseloSzavazatResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SzavazatOfKepviseloService {
    @Autowired
    private SzavazasService szavazasService;

    public KepviseloSzavazatResponse getSzavzatOfKepviselo(String szavazasId, String kepviseloId){
        Szavazas szavazas = szavazasService.findBySzavazasId(szavazasId);
        if(kepviseloDidNotVoteInSzavazas(szavazas.getSzavazatok(), kepviseloId)){
            throw new KepviseloDidNotVoteInSzavazas("Kepviselo by id " + kepviseloId + " did not vote in szavazas");
        }
        return new KepviseloSzavazatResponse(getSzavazatOfKepviseloInSzavazas(szavazas.getSzavazatok(), kepviseloId));
    }

    private boolean kepviseloDidNotVoteInSzavazas(List<Szavazat> szavazatok, String kepviseloId){
        for(Szavazat szavazat : szavazatok){
            if(szavazat.getKepviselo().equals(kepviseloId)){
                return false;
            }
        }
        return true;
    }

    private String getSzavazatOfKepviseloInSzavazas(List<Szavazat> szavazatok, String kepviseloId){
        String kepviseloSzavazat = "";
        for(Szavazat szavazat : szavazatok){
            if(szavazat.getKepviselo().equals(kepviseloId)){
                kepviseloSzavazat = szavazat.getSzavazat().name();
                return kepviseloSzavazat;
            }
        }
        return kepviseloSzavazat;
    }
}
