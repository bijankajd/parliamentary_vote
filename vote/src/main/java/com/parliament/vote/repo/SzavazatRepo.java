package com.parliament.vote.repo;

import com.parliament.vote.domain.Szavazas;
import com.parliament.vote.domain.Szavazat;
import com.parliament.vote.enums.SzavazatTipus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SzavazatRepo extends JpaRepository<Szavazat, Long> {
    List<Szavazat> findBySzavazasAndSzavazat(Szavazas szavazas, SzavazatTipus szavazat);
}
