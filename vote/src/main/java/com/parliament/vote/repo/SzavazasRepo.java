package com.parliament.vote.repo;

import com.parliament.vote.domain.Szavazas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public interface SzavazasRepo extends JpaRepository<Szavazas, Long> {

    Optional<Szavazas> findByIdopont(LocalDateTime idopont);
    Optional<Szavazas> findBySzavazasId(String szavazasId);
}
